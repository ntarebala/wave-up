New in 3.2.14
★ Fix another stupid bug (QuickSettings Tile not working) introduced in 3.2.12.

New in 3.2.13
★ Fix a stupid bug introduced in 3.2.12

New in 3.2.12
★ Android 12 compatibility.
★ Upgrade a bunch of dependencies.

New in 3.2.11
★ Upgrade a bunch of dependencies.

New in 3.2.10
★ Update some translations.
★ Upgrade some dependencies.

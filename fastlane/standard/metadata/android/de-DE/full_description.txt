WaveUp ist eine App, die <i>dein Handy aufweckt</i> - das Display einschaltet - wenn du über den Näherungssensor <i>winkst</i>.

Ich habe diese App entwickelt, um zu vermeiden, immer den Power Button drücken zu müssen, nur um auf die Uhr zu schauen - was ich sehr oft mache. Es gibt schon einige Apps, um genau dies zu tun - und teilweise sogar noch viel mehr. Ich würde inspiriert durch Gravity Screen In/Off, was eine <b>großartige</b> App ist. Dennoch bin ich ein großer Fan von Open Source Software und versuche freie Software so oft wie möglich zu installieren. Ich habe keine Open Source App hierfür gefunden, deshalb habe ich einfach selbst eine geschrieben. Bei Interesse einfach den Source Code anschauen...
https://gitlab.com/juanitobananas/wave-up

Einfach mit der Hand über den Näherungssensor wischen, um das Display einzuschalten. Ich habe das <i>Winkmodus</i> genannt. Diese Funktion kann in den Einstellungen deaktiviert werden, um ein unabsichtliches einschalten zu vermeiden.

Außerdem wird die App das Display einschalten, wenn das Handy aus der Tasche genommen wird. Dies heißt <i>Taschenmodus</i> und kann auch in den Einstellungen deaktiviert werden.

Beide Modi sind automatisch aktiviert.

Die App sperrt auch das Handy und schaltet das Display aus, wenn der Näherungssensor für eine Sekunde (oder eine andere festgelegte Zeit) bedeckt wird. Diese Funktion hat zwar keinen Namen, kann aber auch in den Einstellungen verändert werden. Außerdem ist diese Funktion nicht von Anfang an aktiviert.

Für die, die nicht wissen, was der Näherungssensor ist: Es ist dieser kleine Sensor, der irgendwo am oberen Displayrand ist und dafür sorgt, dass das Handy während eines Anrufes aus ist.

<b>Deinstallieren</b>

Die App ist ein Geräteadministratior und kann deshalb nicht "normal" deinstalliert werden.

Um die App zu deinstallieren, öffnen und im Menü "Deinstallieren" wählen.

<b>Bekannte Probleme</b>

Leider lassen manche Smartphones die CPU aktiv, während der Näherungssensor genutzt wird. Dies wird ein <i>wake lock</i> genannt und verursacht erheblichen Akkuverbrauch. Das ist nicht mein Fehler und ich kann nix dagegen machen. Andere Handys gehen in den "Tiefschlaf" wenn der Bildschirm ausgeschaltet ist, obwohl der Sensor genutzt wird. In diesem Fall ist der Akkuverbrauch praktisch Null-

<b>Erforderliche Android Berechtigungen:</b>

▸ WAKE_LOCK um den Bildschirm einzuschalten
▸ USES_POLICY_FORCE_LOCK um das Gerät zu sperren
▸ RECEIVE_BOOT_COMPLETED um automatisch beim Neustart zu starten
▸ READ_PHONE_STATE um WaveUp während eines Anrufes zu pausieren

<b>Sonstige Bemerkungen</b>

Dies ist meine erste Android App, also Vorsicht!

Dies ist ebenso mein erster kleiner Beitrag für die OpenSource Welt. Endlich!

Es wäre sehr lieb wenn du mir Feedback jeglicher Art oder Unterstützung zukommen lässt!

Danke für's Lesen!

OpenSource rockt!!!

<b>Übersetzungen</b>

Es wäre wirklich cool, wenn du bei der Übersetzung von WaveUp in deiner Sprache helfen kannst (selbst die Englische Version müsste vermutlich kontrolliert werden).
Für Übersetzungen sind zwei Projekte bei Transifex verfügbar: https://www.transifex.com/juanitobananas/waveup/ und https://www.transifex.com/juanitobananas/libcommon/.

<b>Danksagungen</b>

Mein spezieller Dank geht an:

Siehe: https://gitlab.com/juanitobananas/wave-up/#acknowledgments